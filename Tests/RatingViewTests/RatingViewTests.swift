    import XCTest
    @testable import RatingView

    final class RatingViewTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertNotEqual(RatingView(rating: .constant(4), ratingMask: Star(), horizontalSpace: 4).rating, RatingView(rating: .constant(5), ratingMask: Star(), horizontalSpace: 4).rating)
        }
    }
