//
//  Star.swift
//  Red
//
//  Created by Anton on 03.06.2021.
//

import SwiftUI

public struct Star: Shape {
     var sides: Int = 5
     var innerRadiusDivider: CGFloat = 5
     var outerRadiusDivider: CGFloat = 2
    
    /// Description
    /// - Parameters:
    ///   - sides: set  the number of sides for the star
    ///   - innerRadiusDivider: determine the convexity of the inner corners of the star
    ///   - outerRadiusDivider: determine the radius of the inscribed circle relative to the side of the square
    public init(sides: Int = 5,
                innerRadiusDivider: CGFloat = 5,
                outerRadiusDivider: CGFloat = 2) {
        self.sides = sides
        self.innerRadiusDivider = innerRadiusDivider
        self.outerRadiusDivider = outerRadiusDivider
    }

    public func path(in rect: CGRect) -> Path {
        var path = Path()
        let startAngle = CGFloat(-1 * (360 / sides / 4))
        let adjustment = startAngle + CGFloat(360 / sides / 2)
        let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
        let innerPolygon = polygonPointArray(sides: sides,
                                             x: center.x,
                                             y: center.y,
                                             radius: rect.width / innerRadiusDivider,
                                             adjustment: startAngle)
        let outerPolygon = polygonPointArray(sides: sides,
                                             x: center.x,
                                             y: center.y,
                                             radius: rect.width / outerRadiusDivider,
                                             adjustment: adjustment)
        let points = zip(innerPolygon, outerPolygon)
        path.move(to: innerPolygon[0])
        points.forEach { innerPoint, outerPoint in
            path.addLine(to: innerPoint)
            path.addLine(to: outerPoint)
        }
        path.closeSubpath()
        return path
    }

    private func degree2Radian(_ degree: CGFloat) -> CGFloat {
        return CGFloat.pi * degree / 180
    }

    private func polygonPointArray(sides: Int,
                           x: CGFloat,
                           y: CGFloat,
                           radius: CGFloat,
                           adjustment: CGFloat = 0) -> [CGPoint]
    {
        let angle = degree2Radian(360 / CGFloat(sides))
        return Array(0 ... sides).map { side -> CGPoint in
            let adjustedAngle: CGFloat = angle * CGFloat(side) + degree2Radian(adjustment)
            let xpo = x - radius * cos(adjustedAngle)
            let ypo = y - radius * sin(adjustedAngle)
            return CGPoint(x: xpo, y: ypo)
        }
    }
}
