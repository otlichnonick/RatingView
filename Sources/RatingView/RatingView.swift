//
//  RatingView.swift
//  swiftui_components
//
//  Created by Anton on 16.07.2021.
//

import SwiftUI

/// A struct  for displaying the rating by default with 5 elements, red color for filled element and gray color for not filled element
public struct RatingView<RatingMask: Shape>: View {
    @Binding  var rating: Double
    var ratingMask: RatingMask
    var ratingElementCount: Int
    var horizontalSpace: CGFloat
    var filledColor: Color
    var notFilledColor: Color
    var onDidRate: ((Int) -> Void)
    
    /// Description
    /// - Parameters:
    ///   - rating: binding double value of rating
    ///   - ratingMask: the form of the element for the rating
    ///   - ratingElementCount: the count of elements for the rating
    ///   - horizontalSpace: horizontal distance between rating elements
    ///   - filledColor: color for the filled rating element
    ///   - notFilledColor: color for the not filled rating element
    ///   - onDidRate: the action that occurs when you click on the rating element
    public init(rating: Binding<Double>,
                ratingMask: RatingMask,
                ratingElementCount: Int = 5,
                horizontalSpace: CGFloat = 5,
                filledColor: Color = .red,
                notFilledColor: Color = .gray,
                onDidRate: @escaping ((Int) -> Void) = {_ in}) {
        self._rating = rating
        self.ratingMask = ratingMask
        self.ratingElementCount = ratingElementCount
        self.horizontalSpace = horizontalSpace
        self.filledColor = filledColor
        self.notFilledColor = notFilledColor
        self.onDidRate = onDidRate
    }
    
    public var body: some View {
        HStack(spacing: horizontalSpace) {
            ForEach(0 ..< ratingElementCount) { index in
                RatingElementView(ratingMask: ratingMask,
                                  fillAmount: self.getFillAmount(forIndex: index),
                                  filledColor: filledColor,
                                  notFilledColor: notFilledColor)
                    .onTapGesture(count: 1) {
                        self.didRate(index + 1)
                    }
            }
        }
    }
    
    private func didRate(_ rate: Int) {
        onDidRate(rate)
    }
    
    private func getFillAmount(forIndex index: Int) -> Double {
        let calc = rating - Double(index + 1)
        if calc >= 0.0 {
            return 1.0
        } else if calc <= 0.0 && calc > -1.0 {
            return rating - Double(index)
        } else {
            return 0.0
        }
    }
}
